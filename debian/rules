#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2011-2014, 2017 Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for compass-slickmap
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include /usr/share/cdbs/1/rules/upstream-tarball.mk
include /usr/share/cdbs/1/rules/utils.mk
include /usr/share/cdbs/1/rules/debhelper.mk

pkg = $(DEB_SOURCE_PACKAGE)
proj = compass-slickmap

DEB_UPSTREAM_URL = https://github.com/tdreyno/compass-slickmap/archive
DEB_UPSTREAM_TARBALL_BASENAME = v$(DEB_UPSTREAM_TARBALL_VERSION)
DEB_UPSTREAM_TARBALL_MD5 = c2976da754a8e2870254960452405e4a

# needed during build
bdeps = gem2deb

# needed during build and at runtime
deps = ruby | ruby-interpreter
deps +=, ruby-sass (>= 3.1)

CDBS_BUILD_DEPENDS +=, $(bdeps), $(deps)
CDBS_DEPENDS_$(pkg) = $(deps)

configure/$(pkg):: debian/stamp-ruby-gem-configure
debian/stamp-ruby-gem-configure:
	dh_ruby --configure
	touch $@

build/$(pkg):: debian/stamp-ruby-gem-build
debian/stamp-ruby-gem-build:
	dh_ruby --build
	touch $@

install/$(pkg):: debian/stamp-ruby-gem-install
debian/stamp-ruby-gem-install:
	dh_ruby --install
	touch $@

clean::
	dh_ruby --clean
	rm -f $(patsubst %,debian/stamp-ruby-gem-%,configure build install)

# avoid bogusly installing Sass files as Ruby code
binary-post-install/$(pkg)::
	rm -rf $(cdbs_curdestdir)usr/lib/*/*/$(proj)/stylesheets
	rm -rf $(cdbs_curdestdir)usr/lib/*/*/$(proj)/templates

common-binary-fixup-indep::
	chmod -R a-x,a+X $(cdbs_curdestdir)/usr/share/compass/frameworks/*
